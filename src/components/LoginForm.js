import React from 'react'
import auth from '../firebase'
import '../App.css'

class LoginForm extends React.Component {
  constructor(props) 
  {
    super(props)

    this.state = { email: '',password: '', message: '',currentUser: null }
  }

  componentDidMount=()=> 
  {
    auth.onAuthStateChanged(user => 
      {
      if (user) 
      {
        this.setState({ currentUser: user })
      }
    })
  }

  onChange = e => 
  {
    const { name, value } = e.target
    this.setState({[name]: value})
  }

  onSubmit = e => 
  {
    e.preventDefault()

    const { email, password } = this.state
    auth
      .signInWithEmailAndPassword(email, password)
      .then(response => {this.setState({currentUser: response.user })
      })
      .catch(error => { this.setState({message: error.message })
      })
  }

  logout = e => 
  {
    e.preventDefault()
    auth.signOut().then(response => { this.setState({ currentUser: null  })
    })
  }

  render() {
    const { message, currentUser } = this.state

    if (currentUser) {
      return (
        <div>

      <div className="container">
      <div class="row">
      <div class="col-2"></div>
      <div class="col-8 ">
      <br/><h2 className="p-3 mb-2 bg-light text-dark"><strong>Hello, {currentUser.email}</strong></h2>
      </div>
      <div class="col-2"></div>
      </div>
      </div><br/>
        

          <h3> #React-APP run build Production#</h3><br/>


          <div className="container" align="center">
      <div class="row">
      <div class="col-2"></div>
      <div class="col-8 ">
     
      <br/>
      <ul>
      <li>

      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Hello_test/ss.png" alt="Hello-React-App" height="800" width="600"></img>
      <a href="https://gtfarng-reactapp-id-1.netlify.com/">
      <br/><br/><h4>Hello-React-App</h4></a>
      
      </li>
      <br/>
      <li> 
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Counter_v1/ss.png" alt="Counter-React-App" height="800" width="600"></img>
      <a href="https://counter-react-app-ba5bd.firebaseapp.com/"> 
      <br/><br/><h4>Counter-React-App</h4></a>
      </li><br/>
      
      <li> 
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Todo-tasklist_v1/ss.png" alt="Todo-React-App" height="800" width="600"></img>
      <a href="https://todo-react-app-8c02b.firebaseapp.com/"> 
      <br/><br/><h4>Todo-React-App</h4></a>
      </li><br/>

      <li> 
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Github_API_v1/ss.png" alt="Github_API-React-App" height="800" width="600"></img>
        <a href="https://github-react-app-e7294.firebaseapp.com/"> 
      <br/><br/><h4>Github_API-React-App</h4></a>
      </li>
      <br/>
      <li> 
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/counter%26todo%26github_demo/ss.png" alt="My-App1" height="800" width="600"></img>
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/counter%26todo%26github_demo/ss1.png" alt="My-App2" height="800" width="600"></img>
      <a href="https://deploy-react-firebase-f6e56.firebaseapp.com/"> 
      <br/><br/><h4>My-App</h4></a>
      </li>
      <br/>
      <li> 
      <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Github_profile_build/ss.png" alt="Github_Search_Profile-React-App" height="800" width="600"></img>
      <a href="https://gtfarng-github-axios.netlify.com/"> 
      <br/><br/><h4>Github_Search_Profile-React-App</h4></a>
      </li>
      <br/>
    <li> 
    <img src="https://raw.githubusercontent.com/gtfarng/demo-reactapp/master/Weather_build/ss.png" alt="Weather-React-App" height="800" width="600"></img>
     <a href="https://gtfarng-weather-axios.netlify.com/"> 
    <br/><br/><h4>Weather-React-App</h4></a>
    </li>
    <br/>
    </ul>


      </div>
      <div class="col-2"></div>
      </div>
      </div>


          
          
          <div>
    
    </div>
      
      
      
      <br/><br/>
    <div className="container">
      <div class="row">
      <div class="col-2"></div>
      <div class="col-8 ">
      <button class="btn btn-primary btn-lg btn-block control" onClick={this.logout}>Logout</button>
      </div>
      <div class="col-2"></div>
      </div>
      </div><br/>
      
      
    



          
        </div>
      )
    }

    return (

      <div>
      <div className="container">
      <div class="row">
      <div class="col-2"></div>
      <div class="col-8 ">
      <br/><h1 className="p-3 mb-2 bg-light text-dark"><strong>#GTfarng System</strong></h1>
      </div>
      <div class="col-2"></div>
      </div>
      </div>

      <br/><br/>
      <div className="container">
      <div class="row">
      <div class="col-2"></div>
      <div class="col-8 ">
      
      <form onSubmit={this.onSubmit}>
              <div>
              
                
                <div className="control">
                  <input  className="input"  
                          type="email"
                          name="email"
                          placeholder="UserName"
                          onChange={this.onChange} />
                </div>
              </div>
              <br/>
              <div>
                
                <div className="control">
                  <input
                    className="input"
                    type="password"
                    name="password"
                    placeholder="Password"
                    onChange={this.onChange}
                  />
                </div>
              </div>
              <br/><br/>

              {message ? <p className="help is-danger">{message}</p> : null}

              <div>
                <div >
                  <button class="btn btn-primary btn-lg btn-block control" >Login</button>
                </div><br/>
                
              </div>
            </form>

      
      </div>
      <div class="col-2"></div>
      </div>
      </div>


              
  
      </div>
    )
  }
}

export default LoginForm
